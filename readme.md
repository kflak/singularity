# Singularity - WIP

This is the source code for the visuals of [RoosnaFlak's](https://roosnaflak.com) new performance [Singularity](https://roosnaflak.com/performances/singularity/). Very much a work in progress, enter at own risk. Do feel free to scavenge the code for your own creative projects! Released under the [Peer Production License](http://wiki.p2pfoundation.net/Peer_Production_License#Source)
