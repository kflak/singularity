#include "MiniBeeData.h"

MiniBeeData::MiniBeeData(){}

void MiniBeeData::setup(){
    port = 12345;
    for (int i = 0; i < numMBs; i++) {
        MiniBee mb;
        minibee.push_back(mb);
        prevValues.push_back(mb);
        int t = ofGetElapsedTimeMillis();
        t0.push_back(t);
        t1.push_back(t);
    };

    for (auto i : minibee) { i.setup(); };
    for (auto i : prevValues) { i.setup(); };

    receiver.setup(port);
    deltaAlpha = 0.98;
    xAlpha = 0.9999;
    yAlpha = 0.9999;
    zAlpha = 0.9999;
}

void MiniBeeData::update(){
    while( receiver.hasWaitingMessages() ){
        ofxOscMessage m;
        receiver.getNextMessage(m);

        string address = ofToString("/minibee/data");
        if(m.getAddress() == address){
            int id = m.getArgAsInt(0);
            int idx = id - idOffset;
            float x = m.getArgAsFloat(1);
            float y = m.getArgAsFloat(2);
            float z = m.getArgAsFloat(3);

            minibee[idx].setX( x );
            minibee[idx].setY( y );
            minibee[idx].setZ( z );

            // copy over to vectors
            vec3 current, prev;
            current.x = minibee[idx].getX();
            current.y = minibee[idx].getY();
            current.z = minibee[idx].getZ();
            prev.x = prevValues[idx].getX();
            prev.y = prevValues[idx].getY();
            prev.z = prevValues[idx].getZ();

            float prevDelta = prevValues[idx].getDelta();

            // stash away the data for next round of calculations
            prevValues[idx].setX( x );
            prevValues[idx].setY( y );
            prevValues[idx].setZ( z );

            // set the timestamp
            t0[idx] = ofGetElapsedTimeMillis();
            t1[idx] = ofGetElapsedTimeMillis();

            float delta = this->calcDelta(current, prev);
            minibee[idx].setDelta(delta);
            prevValues[idx].setDelta(delta);

            float deltaSmooth = this->calcSmooth(delta, prevDelta, deltaAlpha);
            minibee[idx].setDeltaSmooth(deltaSmooth);

            float xSmooth = this->calcSmooth(x, prev.x, xAlpha);
            minibee[idx].setXSmooth(xSmooth);

            float ySmooth = this->calcSmooth(y, prev.y, yAlpha);
            minibee[idx].setYSmooth(ySmooth);

            float zSmooth = this->calcSmooth(z, prev.z, zAlpha);
            minibee[idx].setZSmooth(zSmooth);

        }
    };
}

MiniBee& MiniBeeData::getMiniBee(int id){
    return minibee[id - idOffset];
}

float MiniBeeData::calcDelta(vec3 current, vec3 previous){

    vec3 deltas;
    deltas = current - previous;
    deltas = abs(deltas);

    float delta = 0;

    for (int i = 0; i < 3; i++){
        delta += deltas[i];
    };

    delta = ofClamp(delta / 3, 0.0, 1.0);

    return delta;
}

float MiniBeeData::calcSmooth(float current, float previous, float alpha){

    return current + alpha * (previous - current);

}

bool MiniBeeData::isConnected(int id){
    int idx = id - idOffset;
    return t1[idx] - t0[idx] > timeOut;
}
