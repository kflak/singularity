#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetVerticalSync(true);
    mb.setup();
    img.load("trees/IMG_8586.png");
    current = PHYSARUM;
    physarum.setup(&mbData);

}

void ofApp::setupFloor(){
    ofBackground(0);
}

//--------------------------------------------------------------
void ofApp::update(){

    mb.update();

    switch(current) {
        case NUMBER_FLOCK:
            numberFlock.update();
            break;
        case L_SYSTEM:
            lSystem.update();
            break;
        case BARREL_DIST:
            barrelDist.update();
            break;
        case SHADER:
            shader.update();
            break;
        case IMG2POLY:
            img2poly.update();
            break;
        case PHYSARUM:
            physarum.update();
            break;
        case BLACK:
            break;
    }

}

//--------------------------------------------------------------
void ofApp::draw(){

    switch(current) {
        case NUMBER_FLOCK:
            numberFlock.draw();
            break;
        case L_SYSTEM:
            lSystem.draw();
            break;
        case BARREL_DIST:
            barrelDist.draw();
            break;
        case SHADER:
            shader.draw();
            break;
        case IMG2POLY:
            img2poly.draw();
            break;
        case PHYSARUM: 
            physarum.trailmap
                .getTexture()
                .drawSubsection(0, 0, ofGetWidth(), ofGetHeight(), 0, 0 );
            break;
        case BLACK:
            ofSetBackgroundColor(0);
            break;
    }
}

void ofApp::drawFloor( ofEventArgs& args ){

    switch(current) {
        case NUMBER_FLOCK:
            numberFlock.draw();
            break;
        case L_SYSTEM:
            lSystem.draw();
            break;
        case BARREL_DIST:
            barrelDist.draw();
            break;
        case SHADER:
            shader.draw();
            break;
        case IMG2POLY:
            img2poly.draw();
            break;
        case PHYSARUM: 
            physarum.trailmap
                .getTexture()
                .drawSubsection( 0, 0, ofGetWidth(), ofGetHeight(), 0, ofGetHeight() );
            break;
        case BLACK:
            ofSetBackgroundColor(0);
            break;
    }
}

void ofApp::exit(){
    ofLog() << "Ciao, my friend!";
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    switch(key){
        case 'q':
            ofExit();
            break;
        case ' ':
            current++;
            break;
        case '1':
            current = NUMBER_FLOCK;
            numberFlock.setup(&mbData);
            break;
        case '2':
            current = L_SYSTEM;
            lSystem.setup(&mbData);
            break;
        case '3':
            current = BARREL_DIST;
            barrelDist.setup();
            break;
        case '4':
            current = SHADER;
            shader.setup();
            break;
        case '5':
            current = IMG2POLY;
            img2poly.setup();
            break;
        case '6': 
            current = PHYSARUM;
            physarum.useImage = false;
            physarum.useSphere = false;
            physarum.wrap = false;
            physarum.particleMode = PHYS_CIRCLE;
            physarum.setup(&mbData);
            break;
        case 'a':
            physarum.resetParticles(PHYS_CIRCLE);
            break;
        case 's':
            physarum.resetParticles(PHYS_HORIZON);
            break;
        case 'd':
            physarum.resetParticles(PHYS_BOTTOMLINE);
            break;
        case 'f':
            physarum.resetParticles(PHYS_TOPLINE);
            break;
        case 'g':
            physarum.resetParticles(PHYS_NOISE);
            break;
        case 'z':
            {
                float h = ofRandom(0.0, 1.0);
                physarum.setParticleRandomHeight( ofGetHeight() - 10 );
                break;
            }
        case 'x':
            {
                float w = ofRandom(0.0, 1.0);
                physarum.setParticleRandomWidth( ofGetWidth() - 10 );
                break;
            }
        case '0':
            current = BLACK;
            break;
        case 'k':
            lSystem.incTrailAmount(0.01);
            break;
        case 'j':
            lSystem.incTrailAmount(-0.01);
            break;
        case 'i': 
            img2poly.newImage();
            break;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
