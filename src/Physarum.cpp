#include "Physarum.h"

Physarum::Physarum(){
    mbData = nullptr;
}

void Physarum::setup(MiniBeeData * mbData_){ 

    mbData = mbData_;

    for(int i = 0; i < numParticles; i++){
        p[i].wrapMode = wrap;
        p[i].setup(particleMode);
    }

    int w = ofGetWidth();
    // int h = ofGetHeight();
    int h = ofGetHeight();

    trailmap.allocate( w, h*2, GL_RGB );
    // backWall.allocate( w, h, GL_RGB );
    // floor.allocate( w, h, GL_RGB );
    pixBuffer.allocate( w, h*2, OF_IMAGE_GRAYSCALE);
    texBuffer.allocate( w, h*2, GL_RGB );


    if ( useImage ){
        // populate a vector full of images
        ofDirectory dir("trees");
        dir.allowExt("png");
        dir.listDir();
        imageDir = dir.getFiles();
        // load 10 random images from the directory
        for (int i = 0; i < 10; i++) {
            int index = ofRandom(imageDir.size());
            ofImage img;
            img.load( imageDir[index].path() );
            img.resize( w, h );
            images.push_back( img );
        };

        if(images[0].isAllocated()) pixBuffer = images[0].getPixels();
    }

    shader.load("shader/physarum");

    maxSpeed = 4.0;
    senseDist = 5.0;

    gui.setup();
    gui.add(numDirs.setup("Number of Directions", 3, 1, 64));
    gui.add(depositAmt.setup("Deposit Amount", 170, 0, 255));
    // gui.add(maxSpeed.setup("Max Speed", 4.0, 0, 10));
    gui.add(maxAngle.setup("Max Angle", 360.0, 0.0, 360.0));
    gui.add(decayRate.setup("Decay Rate", 0.9, 0.0, 1.0));
    // gui.add(senseDist.setup("Sense Distance", 5.0, 0.0, 200.0));
    gui.add(r.setup("Red", 255, 0, 255));
    gui.add(g.setup("Green", 200, 0, 255));
    gui.add(b.setup("Blue", 40, 0, 255));
    gui.add(a.setup("Alpha", 255, 0, 255));

    // sphere.setRadius( w );
    sphere.setRadius( w / 2.0 );
    sphereX = 0.0;
    sphereY = 0.0;
    sphereZ = 0.0;
    sphereXSpeed = 0.0;
    sphereYSpeed = 0.5;
    sphereZSpeed = 0.0;

    t0 = t1 = ofGetElapsedTimeMillis();
}

void Physarum::update(){

    // int w = ofGetWidth();
    // int h = ofGetHeight();

    int mbId = 9;

    if( useImage ){
        float delta;
        if ( mbData->isConnected( mbId ) ) {
            delta = mbData->getMiniBee( mbId ).getDelta();
        } else {
            delta = ofNoise(ofGetElapsedTimef() * 0.2) * 0.15;
        }
        float threshold = 0.1;
        int speedLim = 1000;
        t1 = ofGetElapsedTimeMillis();
        if(delta > threshold && t1 - t0 > speedLim){
            int index = ofRandom(images.size());
            if(images[index].isAllocated()) pixBuffer = images[index].getPixels();
            t0 = t1;
        }
    }

    if (  mbData->isConnected( mbId ) ){
        maxSpeed = mbData->getMiniBee( mbId ).getDeltaSmooth();
        maxSpeed = ofMap(maxSpeed, 0., 1., 0.1, 50.0);
        senseDist = mbData->getMiniBee( mbId ).getDeltaSmooth();
        senseDist = ofMap(senseDist, 0., 1., 3, 200);
    } else {
        maxSpeed += ofSignedNoise(ofGetElapsedTimef());
        maxSpeed = ofClamp(maxSpeed, 0.0, 10.0);
        senseDist += ofSignedNoise(ofGetElapsedTimef() * 0.3) * 2;
        senseDist = ofClamp(senseDist, 0.0, 200.0);
    }

    for (int i = 0; i < numParticles; i++) {
        p[i].setMaxSpeed(maxSpeed);
        p[i].setMaxAngle(maxAngle);
        p[i].setNumDirs(numDirs);
        p[i].setSenseDist(senseDist);
        p[i].setDepositAmt(depositAmt);
        p[i].sense(pixBuffer);
        p[i].move();
        p[i].deposit(pixBuffer);
    }

    texBuffer.loadData(pixBuffer);

    trailmap.begin();
    shader.begin();
    shader.setUniformTexture("tex", texBuffer, 0);
    shader.setUniform1f("dR", decayRate);
    texBuffer.draw(0, 0);
    shader.end();
    trailmap.end();
    trailmap.readToPixels(pixBuffer);
}

void Physarum::draw(){

    ofSetColor(r, g, b, a);

    trailmap.draw(0, 0);

//     if ( useSphere ){
//         ofEnableDepthTest();
//         ofEnableNormalizedTexCoords();
//         trailmap.getTexture().bind();
//         ofNoFill();
//         ofPushMatrix();
//         ofTranslate( w / 2., h / 2. );
//         int mbId = 9;
//         if ( mbData->isConnected( mbId ) ){
//             sphereYSpeed = mbData->getMiniBee( mbId ).getY();
//             sphereYSpeed = ofMap(sphereYSpeed, 0., 1., -0.25, 0.25);
//             sphereXSpeed = mbData->getMiniBee( mbId ).getX();
//             sphereXSpeed = ofMap(sphereXSpeed, 0., 1., 0.25, -0.25);
//         } else {
//             sphereYSpeed = ofSignedNoise(ofGetElapsedTimef()) * 0.25;
//             sphereXSpeed = ofSignedNoise(ofGetElapsedTimef() * 0.3) * 0.25;
//         }
//         sphereY += sphereYSpeed;
//         sphereX += sphereXSpeed;
//         ofRotateXDeg(sphereX);
//         ofRotateYDeg(sphereY);
//         sphere.setPosition(0, 0, 0);
//         sphere.draw();
//         ofPopMatrix();
//         trailmap.getTexture().unbind();
//         ofEnableAlphaBlending();
//         ofDisableDepthTest();
//     } else {
//         // trailmap.draw(0, 0);
//         backWall.draw(0, 0);
//     }

    ofDrawBitmapString(ofToString(ofGetFrameRate()), 20, 20);
    // ofDrawBitmapString(ofToString(y), 20, 40);
    // ofDrawBitmapString(ofToString(x), 20, 60);
    // if(showGui) gui.draw();
}

// void Physarum::drawFloor(){

//     ofSetColor(r, g, b, a);

//     floor.draw(0, 0);

    // if ( useSphere ){
    //     ofEnableDepthTest();
    //     ofEnableNormalizedTexCoords();
    //     trailmap.getTexture().bind();
    //     ofNoFill();
    //     ofPushMatrix();
    //     ofTranslate( w / 2., h / 2. );
    //     int mbId = 9;
    //     if ( mbData->isConnected( mbId ) ){
    //         sphereYSpeed = mbData->getMiniBee( mbId ).getY();
    //         sphereYSpeed = ofMap(sphereYSpeed, 0., 1., -0.25, 0.25);
    //         sphereXSpeed = mbData->getMiniBee( mbId ).getX();
    //         sphereXSpeed = ofMap(sphereXSpeed, 0., 1., 0.25, -0.25);
    //     } else {
    //         sphereYSpeed = ofSignedNoise(ofGetElapsedTimef()) * 0.25;
    //         sphereXSpeed = ofSignedNoise(ofGetElapsedTimef() * 0.3) * 0.25;
    //     }
    //     sphereY += sphereYSpeed;
    //     sphereX += sphereXSpeed;
    //     ofRotateXDeg(sphereX);
    //     ofRotateYDeg(sphereY);
    //     sphere.setPosition(0, 0, 0);
    //     sphere.draw();
    //     ofPopMatrix();
    //     trailmap.getTexture().unbind();
    //     ofEnableAlphaBlending();
    //     ofDisableDepthTest();
    // } else {
    //     floor.draw(0, 0);
    // }

    // ofDrawBitmapString(ofToString(ofGetFrameRate()), 20, 20);
    // ofDrawBitmapString(ofToString(y), 20, 40);
    // ofDrawBitmapString(ofToString(x), 20, 60);
    // if(showGui) gui.draw();
// }

void Physarum::setImage(string p){
    ofImage img;
    img.load(p);
    img.setImageType(OF_IMAGE_GRAYSCALE);
    img.resize( ofGetWidth(), ofGetHeight() );
    pixBuffer = img;
}

void Physarum::resetParticles(int mode = PHYS_CIRCLE){
    for (int i = 0; i < numParticles; i++) {
        p[i].setup(mode);
    }
}
void Physarum::setParticleRandomHeight(float h = 0){
    for (int i = 0; i < numParticles; i++) {
        p[i].randomHeight = h;
    };
    resetParticles(PHYS_RANDOMHORIZON);
}

void Physarum::setParticleRandomWidth(float w = 0){
    for (int i = 0; i < numParticles; i++) {
        p[i].randomWidth = w;
    };
    resetParticles(PHYS_RANDOMVERTICAL);
}
