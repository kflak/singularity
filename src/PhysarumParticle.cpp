#include "PhysarumParticle.h"

PhysarumParticle::PhysarumParticle(){
}

void PhysarumParticle::setup(int mode = PHYS_CIRCLE){

    padding = ceil( senseDist );

    int xpos, ypos;

    switch(mode) {
        case PHYS_CIRCLE:
            {
                float distance = ofRandom(20);
                float angle = ofRandom(ofDegToRad(360.0));
                float xOffset = cos(angle) * distance;
                float yOffset = sin(angle) * distance;
                xpos = ofGetWidth() / 2.0 + xOffset;
                ypos = ofGetHeight() * 1.5  + yOffset;
                break;
            }
        case PHYS_HORIZON:
            {
                float height = 0.5;
                xpos = ofRandomWidth();
                ypos = ofGetHeight() * height;
                break;
            }
        case PHYS_TOPLINE:
            {
                float height = 0.0;
                xpos = ofRandomWidth();
                ypos = ofGetHeight() * height;
                break;

            }
        case PHYS_BOTTOMLINE:
            {
                xpos = ofRandomWidth();
                ypos = ofGetHeight() * 0.8;
                break;

            }
        case PHYS_RANDOMHORIZON:
            {
                xpos = ofRandomWidth();
                ypos = ofGetHeight() * randomHeight;
                break;
            }
        case PHYS_RANDOMVERTICAL:
            {
                xpos = ofGetWidth() * randomWidth;
                ypos = ofRandomHeight() * 2;
                break;
            }
        case PHYS_NOISE:
            {
                xpos = ofRandomWidth();
                ypos = ofRandomHeight() * 2;
                break;
            }

    }

    pos.set(xpos, ypos);
    heading = floor(ofRandom(numDirs));

    depositAmt = 170;
    maxSpeed = 7.8;
    senseDist = 9.2;
    maxAngle = 360.f;

}

void PhysarumParticle::deposit(ofPixels &trailmap){
    float intensity = trailmap.getColor(pos.x, pos.y).getBrightness();
    intensity += depositAmt;
    intensity = ofClamp(intensity, 0, 255);
    ofColor c = ofColor(intensity, 0);
    // ofColor c = ofColor(intensity * r, intensity * g, intensity * b, 255);
    trailmap.setColor(pos.x, pos.y, c);
}

void PhysarumParticle::sense(ofPixels &trailmap){
    padding = ceil( senseDist );
    float nextIntensity = 0;
    float maxIntensity = 0;
    float maxHeading = 0;
    for (int i = -1; i < 2; i++) {
        float look = heading + i;
        float angle = look * (maxAngle / numDirs);
        ofVec2f offset(cos(angle), sin(angle));
        offset.scale(senseDist);

        int currentX, currentY;
        currentX = pos.x + offset.x;
        currentY = pos.y + offset.y;

        if(currentX > ofGetWidth()){
            currentX = padding;
        } else if (currentX < padding){
            currentX = ofGetWidth() - padding;
        }

        if(currentY > ofGetHeight() - padding){
            currentY = padding;
        } else if (currentY < padding){
            currentY = ofGetHeight() - padding;
        }

        nextIntensity = trailmap.getColor(currentX, currentY).r;

        if(maxIntensity < nextIntensity){
            maxIntensity = nextIntensity;
            dir.x = offset.x;
            dir.y = offset.y;
            dir.scale(maxSpeed);
            maxHeading = i;
        }
    }

    heading += maxHeading;
}

void PhysarumParticle::move(){
    pos += dir;
    if ( wrapMode ) {
        wrap();
    } else {
        reset();
    }
}

void PhysarumParticle::wrap(){
    if(pos.x > ofGetWidth()) pos.x = padding;
    if(pos.x < 0 ) pos.x = ofGetWidth() - padding;
    if(pos.y > ofGetHeight() * 2) pos.y = padding;
    if(pos.y < 0) pos.y = ofGetHeight() * 2 - padding;

}

void PhysarumParticle::reset(){
    if(pos.x > ofGetWidth() || pos.x < padding) pos.x = ofRandomWidth() - padding;
    if(pos.y > ofGetHeight() * 2 || pos.y < padding) pos.y = ofRandomHeight() * 2 - padding;
}

void PhysarumParticle::setMaxSpeed(float n){
    maxSpeed = n;
}

void PhysarumParticle::setMaxAngle(float n){
    maxAngle = n;
}

void PhysarumParticle::setSenseDist(float n){
    senseDist = n;
}

void PhysarumParticle::setDepositAmt(float n){
    depositAmt = n;
}

void PhysarumParticle::setNumDirs(int n){
    numDirs = n;
}
