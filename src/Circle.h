#pragma once

#include "ofMain.h"

class Circle {

    public:
        Circle();
        void setup();
        void update();
        void draw();

        float x;
        float y;
        float r;
        bool growing;
};
