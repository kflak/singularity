#pragma once

#include "ofMain.h"

using namespace glm;

class MiniBee {
    public:
        MiniBee();
        float getX();
        float getXSmooth();
        float getY();
        float getYSmooth();
        float getZ();
        float getZSmooth();
        float getDelta();
        float getDeltaSmooth();
        void setup();

        void setX(float in);
        void setXSmooth(float in);
        void setY(float in);
        void setYSmooth(float in);
        void setZ(float in);
        void setZSmooth(float in);
        void setDelta(float in);
        void setDeltaSmooth(float in);

    private:
        vec3 data;
        vec3 dataSmooth;
        float delta;
        float deltaSmooth;
};
