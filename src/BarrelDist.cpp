#include "BarrelDist.h"

BarrelDist::BarrelDist(){};

void BarrelDist::setup(){

    video.load("snowyBackground.mp4");
    // video.load("kulliStoneDance.mp4");
    video.play();


    // image.load("hubble.png");
    // imageReduced.load("hubble.png");
    // float imageScalar = 8;
    // imageReduced.resize(ofGetWidth()/imageScalar, ofGetHeight()/imageScalar);


}

void BarrelDist::update(){

    // mesh.setMode(OF_PRIMITIVE_POINTS);
    mesh.setMode(OF_PRIMITIVE_LINES);
    mesh.enableColors();
    mesh.enableIndices();

    video.update();
    if(video.isFrameNew()){

        mesh.clear();

        float imageScalar = 12;
        ofPixels pixels = video.getPixels();
        pixels.resize(ofGetWidth()/imageScalar, ofGetHeight()/imageScalar);

        float intensityThreshold = 150.0;
        int w = pixels.getWidth();
        int h = pixels.getHeight();
        for (int x = 0; x < w; ++x) {
            for (int y = 0; y < h; y++) {
                ofColor c = pixels.getColor(x, y);
                float intensity = c.getLightness();
                if( intensity >= intensityThreshold ) {
                    float saturation = c.getSaturation();
                    float z = ofMap(saturation, 0, 255, -200, 200);
                    // glm::vec3 pos(x, y, z);
                    glm::vec3 pos(x*imageScalar, y*imageScalar, z);
                    mesh.addVertex(pos);
                    mesh.addColor(c);
                }
            }
        }

        float connectionDistance = 20;
        int numVerts = mesh.getNumVertices();
        for (int a = 0; a < numVerts; a++) {
            glm::vec3 verta = mesh.getVertex(a);
            for (int b = a+1; b < numVerts; b++) {
                glm::vec3 vertb = mesh.getVertex(b);
                float distance = glm::distance(verta, vertb);
                if ( distance <= connectionDistance ) {
                    mesh.addIndex(a);
                    mesh.addIndex(b);
                }
            };

        };

        meshCopy = mesh;
    }

    glm::vec3 mouse(ofGetMouseX(), ofGetMouseY(), 0.0);

    for (int i = 0; i < mesh.getNumVertices(); i++) {
        glm::vec3 vertex = meshCopy.getVertex(i);
        float distanceToMouse = glm::distance(mouse, vertex);
        float displacement = ofMap(distanceToMouse, 0, 400, 300.0, 0, true);
        glm::vec3 direction = vertex - mouse;
        direction = glm::normalize(direction);
        glm::vec3 displacedVertex = vertex + displacement * direction;
        mesh.setVertex(i, displacedVertex);
    }
}

void BarrelDist::draw(){
    // ofBackground(255, 255, 255);
    ofBackground(0, 0, 0);
    // ofPushMatrix();
    // ofTranslate(ofGetWidth()/2, ofGetHeight()/2);
    // ofRotateRad(xRotation);
    // xRotation += xRotationInc;
    mesh.draw();
    // ofPopMatrix();
}
