#pragma once

#include "ofMain.h"

enum PhysarumParticleMode {
    PHYS_CIRCLE,
    PHYS_HORIZON,
    PHYS_TOPLINE,
    PHYS_BOTTOMLINE,
    PHYS_NOISE,
    PHYS_RANDOMHORIZON,
    PHYS_RANDOMVERTICAL,
};

class PhysarumParticle{
    public:
        PhysarumParticle();
        ofVec2f pos;
        ofVec2f dir;

        void setup(int mode);
        void deposit(ofPixels &trailmap);
        void sense(ofPixels &trailmap);
        void move();
        void wrap();
        void reset();

        void setMaxSpeed(float n);
        void setMaxAngle(float n);
        void setDepositAmt(float n);
        void setSenseDist(float n);
        void setNumDirs(int n);

        bool wrapMode = false;

        float randomHeight = 0.1;
        float randomWidth = 0.1;

    private:
        float heading;
        float maxSpeed;
        float maxAngle;
        float depositAmt;
        float senseDist;
        int numDirs = 32;
        int padding;
};
