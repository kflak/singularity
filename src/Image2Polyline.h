#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "Circle.h"

class Image2Polyline {
    public:
        Image2Polyline();
        void setup();
        void update();
        void draw();
        vector<glm::vec2> getCentroids();
        vector<ofPolyline> getPolylines();
        void newImage();

    private:
        ofImage image;
        ofxCvColorImage colorImage;
        ofxCvGrayscaleImage grayImage;
        ofxCvGrayscaleImage grayBg;
        ofxCvGrayscaleImage grayDiff;
        ofxCvContourFinder contourFinder;

        int threshold;
        bool bLearnBackground;

        vector<glm::vec2> centroids;
        vector<ofPolyline> polyLines;
        vector<ofImage> images;
        vector<glm::vec2> circles;
        vector<string> files;
        int imgIndex = 0;
        int prevImgIndex = 0;
};
