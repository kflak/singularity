#include "Line.h"

Line::Line(){}

void Line::setup(float start = 0, float end = 0, float time = 10){
    t0 = ofGetElapsedTimef();
    t1 = t0;
    t = time;
    startValue = start;
    endValue = end;
    currentValue = start;
}

void Line::setup(int start = 0, int end = 0, float time = 10){
    t0 = ofGetElapsedTimef();
    t1 = t0;
    t = time;
    startValue = start;
    endValue = end;
    currentValue = start;
}

void Line::update(){
    t1 = ofGetElapsedTimef();
    float elapsedRatio = (t1 - t0) / t;
    if(elapsedRatio <= 1.0){
        currentValue = ofLerp(startValue, endValue, elapsedRatio);
    }
}

float Line::getValueAsFloat(){
    this->update();
    return currentValue;
}

float Line::getValueAsInt(){
    this->update();
    return int(currentValue);
}
