// Based on Samuel Cho's (https://github.com/chosamuel) implementation of 
// slime mold growth patterns. 
// Original repo here: https://github.com/chosamuel/openframeworks-physarum

#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "PhysarumParticle.h"
#include "MiniBeeData.h"
#include "ofxThreadedImageLoader.h"

class Physarum {
    public:
        Physarum();
        void setup(MiniBeeData * mbData_);
        void resetParticles(int mode);
        void setParticleRandomHeight(float h);
        void setParticleRandomWidth(float w);
        void update();
        void draw();
        void drawBackWall();
        void drawFloor();
        void setImage(string p);

        ofShader shader;
        ofFbo trailmap;
        // ofTexture backWall;
        // ofTexture floor;
        ofPixels pixBuffer;
        ofTexture texBuffer;

        bool showGui = false;
        bool useImage = false;
        bool useSphere = false;
        bool wrap = true;

        static const int numParticles = 50000;
        PhysarumParticle p[numParticles];
        int particleMode = PHYS_CIRCLE;


        ofxPanel gui;
        ofxFloatSlider depositAmt;
        float maxSpeed;
        // ofxFloatSlider maxSpeed;
        ofxFloatSlider maxAngle;
        float senseDist;
        // ofxFloatSlider senseDist;
        ofxFloatSlider decayRate;
        ofxIntSlider numDirs;
        ofxIntSlider r;
        ofxIntSlider g;
        ofxIntSlider b;
        ofxIntSlider a;

    private:
        ofSpherePrimitive sphere;
        float sphereX;
        float sphereY;
        float sphereZ;
        float sphereXSpeed;
        float sphereYSpeed;
        float sphereZSpeed;
        // ofImage img;
        vector<ofImage> images;
        vector<ofFile> imageDir;

        MiniBeeData * mbData;
        int t0, t1;
};
