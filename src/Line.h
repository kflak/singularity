#pragma once

#include "ofMain.h"

class Line{
    public:
        Line();
        void setup(float start, float end, float time);
        void setup(int start, int end, float time);
        void update();
        float getValueAsFloat();
        float getValueAsInt();

    private:
        float t;
        float t0;
        float t1;
        float currentValue;
        float startValue;
        float endValue;
};
