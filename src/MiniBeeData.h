#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "MiniBee.h"

using namespace glm;

class MiniBeeData {

    public:
        MiniBeeData();

        void setup();
        void update();
        void setDeltaAlpha(float alpha);
        void setXAlpha(float alpha);
        void setYAlpha(float alpha);
        float getDeltaAlpha();
        float getXAlpha();
        float getYAlpha();

        bool isConnected(int id);

        MiniBee& getMiniBee(int id);

        int port;
        vector <MiniBee> minibee;
        vector <MiniBee> prevValues;
        const int idOffset = 9;
        const int numMBs = 8;
        const int timeOut = 1000;

    private:
        ofxOscReceiver receiver;
        float calcDelta(vec3 current, vec3 previous);
        float calcSmooth(float current, float previous, float alpha);
        float deltaAlpha;
        float xAlpha;
        float yAlpha;
        float zAlpha;
        vector<int> t0;
        vector<int> t1;
};
