#pragma once

#include "ofMain.h"
#include "MiniBeeData.h"

enum particleMode{
    PARTICLE_MODE_ATTRACT = 0,
    PARTICLE_MODE_REPEL,
    PARTICLE_MODE_NEAREST_POINTS,
    PARTICLE_MODE_NOISE
};

class Particle{

    public:
        Particle();

        void loadImage(string fileName );
        void setMode(particleMode newMode);

        void reset();
        void update(MiniBeeData& mbData);
        void draw();		

        glm::vec3 pos;
        glm::vec3 vel;
        glm::vec3 frc;
        glm::vec3 rotVel;

        float degree;

        float drag; 
        float uniqueVal;

        float scale;

        ofImage image;
        int alpha;

        particleMode mode;

        vector <glm::vec3> * attractPoints;
};
