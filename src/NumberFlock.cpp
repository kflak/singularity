#include "NumberFlock.h"

NumberFlock::NumberFlock(){
    mbData = nullptr;
}

void NumberFlock::setup(MiniBeeData * mbData_){

    this->mbData = mbData_;

    background.load("snowyBackground.mp4");
    background.play();

    string path = ofToString( "numbers" );
    ofDirectory dir(path); 
    string ext = ofToString( "png" );
    dir.allowExt(ext);
    dir.listDir();

    int num = 100;

    p.assign(num, Particle());
    for (int i = 0; i < num; i++) {
        int index = int(ofRandom(dir.size()));
        p[i].loadImage(dir.getPath(index));
        p[i].scale = 40;
    };

    this->resetParticles();
}

void NumberFlock::update(){
    background.update();

    for (unsigned int i = 0; i < p.size(); i++) {
        p[i].update(*mbData);
    };
}

void NumberFlock::draw(){

    background.draw(0, 0, ofGetWidth(), ofGetHeight());

    for (int i = 0; i < p.size(); i++) {
        p[i].draw();
    };
}
void NumberFlock::resetParticles(){

    for (unsigned int i = 0; i < p.size(); i++) {
        p[i].reset();
    }
}
