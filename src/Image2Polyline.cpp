#include "Image2Polyline.h"

Image2Polyline::Image2Polyline(){}

void Image2Polyline::setup(){
    files = ofSplitString(ofBufferFromFile("trees.txt").getText(), "\n");
    this->newImage();
}

void Image2Polyline::update(){
    if(image.isAllocated()){ 
        image.resize(ofGetWidth(), ofGetHeight());
        grayImage.allocate(image.getWidth(), image.getHeight());
        grayBg.allocate(image.getWidth(), image.getHeight());
        grayDiff.allocate(image.getWidth(), image.getHeight());

        grayImage.setFromPixels(image.getPixels());
        if (bLearnBackground){
            grayBg = grayImage;
            bLearnBackground = false;
        }

        grayDiff.absDiff(grayBg, grayImage);

        threshold = 100;

        grayDiff.threshold(threshold);
        grayDiff.erode();
        grayDiff.dilate();

        contourFinder.findContours(grayDiff, 100, 25000, 200, false);

        polyLines.clear();
        centroids.clear();

        int numBlobs = contourFinder.nBlobs;
        for (int i = 0; i < numBlobs; i++) {

            ofxCvBlob blob = contourFinder.blobs[i];

            ofPolyline poly = blob.pts;
            polyLines.push_back(poly);

            glm::vec2 centroid = poly.getCentroid2D();
            centroids.push_back(centroid);
        };

        for (int i = 0; i < 10; i++) {
            glm::vec3 point(ofRandom(0, ofGetWidth()), ofRandom(0, ofGetHeight()), 0);
            for (int i = 0; i < polyLines.size(); i++) {
                if (polyLines[i].inside(point.x, point.y)) {
                    circles.push_back(point);
                    break;
                }
            };
        };
    }}

void Image2Polyline::draw(){ 

    ofSetBackgroundColor(255);
    ofSetColor(255, 0, 0);

    for (int i = 0; i < circles.size(); i++) {
        glm::vec2 p(circles[i]);
        ofDrawCircle(p, 3);
    };

    for (int i = 0; i < polyLines.size(); i++) {
        ofPolyline pSmooth = polyLines[i].getSmoothed(5, 0);
        pSmooth.draw();
        glm::vec2 c = centroids[i];
        ofDrawCircle(c, 3);
    };
}

vector<glm::vec2> Image2Polyline::getCentroids(){
    return centroids;
}

vector<ofPolyline> Image2Polyline::getPolylines(){
    return polyLines;
}

void Image2Polyline::newImage(){

        while(imgIndex == prevImgIndex) {
            imgIndex = ofRandom(0, files.size());
        }

        prevImgIndex = imgIndex;

        image.load(files[imgIndex]);

        image.setImageType(OF_IMAGE_GRAYSCALE);

        ofPixels pixels = image.getPixels();
        ofPixels inverted;
        inverted.allocate(image.getWidth(), image.getHeight(), OF_IMAGE_GRAYSCALE);
        int nBytes = image.getWidth() * image.getHeight();
        for (int i = 0; i < nBytes; i++) {
            inverted[i] = 255 - pixels[i];
        };

        image.setFromPixels(inverted);

        circles.clear();
}
