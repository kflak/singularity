#include "MyFirstShader.h"

void MyFirstShader::setup(){
    shader.load("shader/shader");

    video.load("kulliStoneDance.mp4");
    video.play();
    fbo.allocate(video.getWidth(), video.getHeight());
    imageMask.load("img_mask.png");

}

void MyFirstShader::update(){
    video.update();
}

void MyFirstShader::draw(){

    fbo.begin();
    ofClear(255, 255, 255, 255);

    shader.begin();
    shader.setUniformTexture("imageMask", imageMask.getTexture(), 1);
    shader.setUniformTexture("video", video.getTexture(), 2);

    imageMask.draw(0, 0);
    shader.end();

    fbo.end();
    fbo.draw(0, 0);
}
