#include "Particle.h"

//------------------------------------------------------------------
Particle::Particle(){
    attractPoints = NULL;
}

//------------------------------------------------------------------
void Particle::loadImage(string fileName){
    image.load( fileName );
}

void Particle::setMode(particleMode newMode){
    mode = newMode;
};
//------------------------------------------------------------------
void Particle::reset(){
    //the unique val allows us to set properties slightly differently for each particle

    alpha = ofRandom(50, 255);

    uniqueVal = ofRandom(-10000, 10000);

    pos.x = ofRandomWidth() * 2.f;
    pos.y = ofGetHeight() / 3.f;
    pos.z = ofRandom(-10000, -1000);

    vel.x = ofRandom(-3.9, 3.9);
    vel.y = ofRandom(-0.9, 0.9);
    vel.z = ofRandom(10.0, 200.0);

    rotVel.x = ofRandom(-1.0, 1.0);
    rotVel.y = ofRandom(-1.0, 1.0);
    rotVel.z = ofRandom(-1.0, 1.0);

    degree = ofRandom(0, 360);

    frc   = glm::vec3(0,0,0);

    scale = ofRandom(10, 100);

    drag  = ofRandom(0.9, 0.999);
}

//------------------------------------------------------------------
void Particle::update(MiniBeeData& mbData){

    float forceMul = mbData.getMiniBee(9).getDeltaSmooth();
    forceMul = glm::pow(forceMul, 3) * 1000;

    float fakeWindX = ofSignedNoise(pos.x * 0.003, pos.y * 0.006, ofGetElapsedTimef() * 0.6);

    frc.x = fakeWindX * 0.25 + ofSignedNoise(uniqueVal, pos.y * 0.04) * 0.6;
    frc.y = ofSignedNoise(uniqueVal, pos.x * 0.006, ofGetElapsedTimef()*0.02) * 0.09;
    frc.z = ofNoise(uniqueVal, pos.x * 0.006, ofGetElapsedTimef()*0.6) * 0.09;

    vel *= drag; 
    vel += frc * 0.4 * forceMul;

    if( pos.z > 2000 ){
        reset();
    }

    pos += vel; 

}

//------------------------------------------------------------------
void Particle::draw(){

    ofPushMatrix();
    ofTranslate(glm::vec3(ofGetWidth()/2, ofGetHeight()/3, pos.z));
    ofRotateDeg(degree, rotVel.x, rotVel.y, rotVel.z);
    image.setAnchorPoint(image.getWidth()/2, image.getHeight()/2);
    ofEnableAlphaBlending();
    ofSetColor(255, 255, 255, alpha);
    image.draw(pos.x, pos.y, scale, scale);
    ofDisableAlphaBlending();
    degree += rotVel.x;
    degree = fmod (degree, 360);
    pos.z += vel.z;
    ofPopMatrix();
}

