#pragma once

#include "BarrelDist.h"
#include "Image2Polyline.h"
#include "Lsystem.h"
#include "MiniBeeData.h"
#include "MyFirstShader.h"
#include "NumberFlock.h"
#include "Physarum.h"
// #include "FloorProjection.h"
#include "ofMain.h"

enum CurrentPatch {
    NUMBER_FLOCK,
    L_SYSTEM,
    BARREL_DIST,
    SHADER,
    IMG2POLY,
    PHYSARUM,
    BLACK,
    CURRENTPATCH_LAST
};

class ofApp : public ofBaseApp {

    public:
        void setup();
        void setupFloor();
        void update();
        void draw();
        void drawFloor(ofEventArgs & args);
        void exit();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

        shared_ptr<ofAppBaseWindow> floorProjectionWindow;

        MiniBeeData mbData;
        MiniBeeData& mb = mbData;

        NumberFlock numberFlock;
        Lsystem lSystem;
        BarrelDist barrelDist;
        MyFirstShader shader;
        Image2Polyline img2poly;
        // shared_ptr<Physarum> physarum;
        Physarum physarum;

        ofImage img;
        // int w, h;

        // ofFbo fbo;
        // ofPixels pixels;
        // ofPixels tempPix;
        // ofTexture texture;
        // Param param;
        // Param& p = param;

        int current;
        ofXml xml;
};
