#include "ofMain.h"

class MyFirstShader{

    public:
        void setup();
        void update();
        void draw();

    private:
        ofVideoPlayer video;
        ofFbo fbo;
        ofImage imageMask;
        ofShader shader;
};
