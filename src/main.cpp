#include "ofMain.h"
#include "ofApp.h"
#include "ofAppGLFWWindow.h"

int main( ){

    ofGLFWWindowSettings settings;
    settings.setGLVersion(3, 2);
    settings.setPosition( vec2(0, 0) );
    settings.setSize(1920, 1080);
    settings.title = "Singularity BackWall";
    // settings.windowMode = OF_WINDOW;
    settings.windowMode = OF_FULLSCREEN;


    auto wallProjectionWindow = ofCreateWindow( settings );

    settings.setSize(1920, 1080);
    settings.title = "Singularity Floor";
    // settings.windowMode = OF_WINDOW;
    settings.windowMode = OF_FULLSCREEN;
    settings.shareContextWith = wallProjectionWindow;

    auto app = make_shared<ofApp>();

    app->floorProjectionWindow = ofCreateWindow( settings );
    app->setupFloor();

    ofAddListener( app->floorProjectionWindow->events().draw, app.get(), &ofApp::drawFloor );

    ofRunApp(wallProjectionWindow, app);
    ofRunMainLoop();
}
