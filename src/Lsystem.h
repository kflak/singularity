#pragma once 

#include "ofMain.h"
#include "MiniBeeData.h"
#include "Line.h"

class Lsystem{
    public: 
        Lsystem();
        void setup(MiniBeeData * mbData_);
        void update();
        void draw();
        void generate();
        void incAngle(float a);
        void incTrailAmount(float h);

    private:

        float width;
        float height;

        ofImage background;

        ofFbo fbo;
        float history;

        vector<float> sizes;
        vector<float> lengths;
        vector<float> widths;
        vector<ofColor> circleColors;
        vector<ofColor> lineColors;
        int index;
        int numGenerations;
        string axiom;
        string sentence;
        string subSentence;

        float minLength;
        float maxLength;
        float minSize;
        float maxSize;
        float minWidth;
        float maxWidth;
        float angle;
        float angleInc;
        float yRotation;
        float yRotationInc;
        float xRotation;
        float xRotationInc;
        MiniBeeData * mbData;
        Line fadeIn;
};
