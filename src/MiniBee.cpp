#include "MiniBee.h"

MiniBee::MiniBee(){};

void MiniBee::setup(){
    data = {0.0, 0.0, 0.0};
    delta = 0.0;
    deltaSmooth = 0.0;
    dataSmooth = {0.0, 0.0, 0.0};
}

float MiniBee::getX(){ return data.x; }
float MiniBee::getY(){ return data.y; }
float MiniBee::getZ(){ return data.z; }
float MiniBee::getXSmooth(){ return dataSmooth.x; }
float MiniBee::getYSmooth(){ return dataSmooth.y; }
float MiniBee::getZSmooth(){ return dataSmooth.z; }
float MiniBee::getDelta(){ return delta; }
float MiniBee::getDeltaSmooth(){ return deltaSmooth; }

void MiniBee::setX(float in = 0.0){ 
    data.x = ofMap(in, 0.47020, 0.534, 0.0, 1.0, true); 
}
void MiniBee::setXSmooth(float in = 0.0){
    dataSmooth.x = in;
}
void MiniBee::setY(float in = 0.0){
    data.y = ofMap(in, 0.469, 0.5322, 0.0, 1.0, true); 
}
void MiniBee::setYSmooth(float in = 0.0){
    dataSmooth.y = in;
}
void MiniBee::setZ(float in = 0.0){
    data.z = ofMap(in, 0.469, 0.650, 0.0, 1.0, true); 
}
void MiniBee::setZSmooth(float in = 0.0){
    dataSmooth.z = in;
}
void MiniBee::setDelta(float in = 0.0){
    delta = in; 
}
void MiniBee::setDeltaSmooth(float in = 0.0){
    deltaSmooth = in; 
}
