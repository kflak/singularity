#pragma once 

#include "ofMain.h"
#include "Particle.h"
#include "MiniBeeData.h"

class NumberFlock{

    public:
        NumberFlock();
        void setup(MiniBeeData * mbData_);
        void update();
        void draw();

        ofVideoPlayer background;

    private:
        void resetParticles(); 
        vector <Particle> p;
        MiniBeeData * mbData;
};
