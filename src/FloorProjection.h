#pragma once
#include "Physarum.h"
#include "ofMain.h"

class FloorProjection : public ofBaseApp {
    public:
        void setup();
        void update();
        void draw();
        void exit();

        shared_ptr<ofBaseApp> wallProjection;
        // shared_ptr<Physarum> physarum;

    private:
};
