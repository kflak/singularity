#include "ofMain.h"

class BarrelDist{

    public:
        BarrelDist();
        void setup();
        void update();
        void draw();

    private:
        ofVideoPlayer video;
        ofImage image;
        ofImage imageReduced;
        ofMesh mesh;
        ofMesh meshCopy;
        ofEasyCam easyCam;
        float xRotation;
        float xRotationInc;
};
