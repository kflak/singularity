#include "Lsystem.h"

Lsystem::Lsystem(){ 
    mbData = nullptr;
}

void Lsystem::setup(MiniBeeData * mbData_){

    this->mbData = mbData_;

    width = ofGetWidth();
    height = ofGetHeight();

    background.load("snowyBackground.png");

    axiom = ofToString( "F" );
    sentence = axiom;
    minSize = 5.f;
    maxSize = 20.f;
    minLength = 5.f;
    maxLength = 20.f;
    minWidth = 0.5f;
    maxWidth = 2.f;
    xRotation = 0.f;
    xRotationInc = 0.3;
    yRotation = 20.f;
    yRotationInc = 0.3;
    angle = 20.f;
    angleInc = 0.2;
    numGenerations = 5;


    for (int i = 0; i < numGenerations; i++) { generate(); };

    for (int i = 0; i < sentence.length(); i++){
        float len = ofMap(i, 0.0, sentence.length(), maxLength, minLength) * ofRandom(0.5, 2.0);
        lengths.push_back(len);
        float size = ofRandom(minSize, maxSize);
        sizes.push_back(size);
        float width = ofMap(i, 0.0, sentence.length(), maxWidth, minWidth);
        widths.push_back(width);
        ofColor circleColor = ofColor(0);
        circleColor.setHsb(ofRandom(150, 175), ofRandom(20, 255), ofRandom(20, 255));
        // ofColor circleColor = ofColor(0, ofRandom(25, 255), ofRandom(20, 30)); // greenish
        circleColors.push_back(circleColor);
        ofColor lineColor = ofColor(ofRandom(90, 255));
        lineColors.push_back(lineColor);
    };

    index = sentence.find(']');
    subSentence = sentence.substr(0, index+1);

    fadeIn.setup(0, 125, 30);

    history = 0.97;
    fbo.allocate(width, height);
    // fbo.begin();
    // ofBackground(0, 0, 200);
    // background.draw(0, 0, width, height);
    // ofBackgroundGradient(ofColor(125, 0, 255), ofColor(0,0,0), OF_GRADIENT_CIRCULAR);
    // fbo.end();
}

void Lsystem::update(){

    float mbDelta = mbData->getMiniBee(9).getX();
    history = ofMap(mbDelta, 0.0, 1.0, 0.85, 1.0);
    
    fbo.begin();
    float alpha = (1-history) * 255;
    ofSetColor(0, 0, 255, alpha);
    ofFill();
    ofDrawRectangle(0, 0, width, height);

    ofPushMatrix();
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    ofRotateXDeg(xRotation);
    xRotation += xRotationInc;

    float transY = mbData->getMiniBee(9).getY() * 360;
    int numOpenBrackets=0;
    int numClosedBrackets=0;

    for (int i = 0; i < subSentence.length(); i++) {
        char current = sentence[i];
        switch(current){
            case 'F':
                ofSetLineWidth(widths[i]);
                ofSetColor(lineColors[i]);
                // ofSetColor(ofColor(ofRandom(125, 255)));
                // ofSetColor(ofColor(125, 100, 50));
                ofDrawLine(0, 0, 0, -lengths[i]);
                ofTranslate(0, -lengths[i]);
                break;
            case '+':
                ofRotateDeg(angle);
                break;
            case '-':
                ofRotateDeg(-angle);
                break;
            case '[':
                ofPushMatrix();
                numOpenBrackets++;
                break;
            case ']':
                // ofSetColor(circleColors[i]);
                // ofDrawCircle(0, 0, sizes[i]);
                ofPopMatrix();
                numClosedBrackets++;
                break;
            case '/':
                ofRotateYDeg(-yRotation);
                break;
            case '|':
                ofRotateYDeg(yRotation);
                break;
        }
    };


    while(numOpenBrackets > numClosedBrackets){
        ofPopMatrix();
        numClosedBrackets++;
    };


    index = sentence.find(']', index+1);
    subSentence = sentence.substr(0, index+1);

    angle += angleInc;
    yRotation += yRotationInc;
    ofPopMatrix();


    fbo.end();
}

void Lsystem::draw(){

    fbo.draw(0, 0);

    int alpha = fadeIn.getValueAsFloat();
    ofSetColor(255, 255, 255, alpha);
    background.draw(0, 0, width, height);
}

void Lsystem::generate(){
    string nextSentence = ofToString("");
    for (int i = 0; i < sentence.length(); i++) {
        char current = sentence[i];
        switch(current){
            case 'F':
                nextSentence += ofToString("FF+[+F/-F-/F-F-|F]-[-F+|F+|F]");
                break;
            default:
                nextSentence += current;
                break;
        }
    };
    sentence = nextSentence;
}

void Lsystem::incAngle(float a=1){
    angle += a;
};

void Lsystem::incTrailAmount(float h=0.01){
    history += h;
}
