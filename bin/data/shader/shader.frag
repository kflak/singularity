#version 150

uniform sampler2DRect imageMask;
uniform sampler2DRect video;

in vec2 texCoordVarying;

out vec4 outputColor;

void main()
{
    vec4 texel0 = texture(video, texCoordVarying);
    vec4 texel1 = texture(imageMask, texCoordVarying);
    float r = 1-texel0.r;
    float g = 1-texel0.g;
    float b = 1-texel0.b;

    outputColor = vec4(r, g, b, texel0.a * texel1.a);
}
