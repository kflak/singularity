#version 150

uniform mat4 modelViewProjectionMatrix;
in vec4 position;
in vec2 texcoord;

uniform sampler2DRect tex0;

out vec2 texCoordVarying;

void main(){

    vec4 pos = modelViewProjectionMatrix * position;

    float displacementY = texture(tex0, texcoord).y * 0.5;

    pos.y += displacementY;

    gl_Position = pos;

    texCoordVarying = texcoord;
}
